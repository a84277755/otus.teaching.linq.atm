﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        public void GetUserInfo(string login, string password)
        {
            User user = Users.SingleOrDefault(x => x.Login == login && x.Password == password);
            if (user != null)
            {
                Console.WriteLine($"{user.FirstName} {user.SurName} {user.MiddleName}");
            }
            else
            {
                Console.WriteLine("null");
            }
        }

        public void GetUserAccounts(int id)
        {
            try
            {
                IEnumerable<Account> accounts = Accounts.Where(x => x.UserId == id);
                foreach (var account in accounts)
                {
                    Console.WriteLine($"[{account.Id}] {account.OpeningDate} {account.CashAll}");
                }
                
            }
            catch
            {
                Console.WriteLine("null");
            }
        }

        public void GetUserAccountsWithHistory(int id)
        {
            try
            {
                var accounts = Accounts
                    .Where(x => x.UserId == id)
                    .Join(
                        History,
                        account => account.Id,
                        history => history.AccountId,
                        (account, history) => $"{account.Id} {account.CashAll} > {history.OperationType} {history.CashSum}"
                     );

                foreach (var account in accounts)
                {
                    Console.WriteLine(account);
                }

            }
            catch
            {
                Console.WriteLine("null");
            }
        }

        public void GetHistoryWithUserDetails()
        {
            try
            {
                var history = History
                    .Where(x => x.OperationType == OperationType.InputCash)
                    .Join(
                        Accounts,
                        history => history.AccountId,
                        account => account.Id,
                        (history, account) => new { history, account }
                     )
                    .Join(
                        Users,
                        historyWithAccount => historyWithAccount.account.UserId,
                        user => user.Id,
                        (historyWithAccount, user) => $"{historyWithAccount.history.OperationType} {historyWithAccount.history.CashSum} > {user.FirstName} {user.SurName}"
                    );
                

                foreach (var record in history)
                {
                    Console.WriteLine(record);
                }

            }
            catch
            {
                Console.WriteLine("null");
            }
        }

        public void GetUsersWithSpecificBalance(decimal balance)
        {
            try
            {
                var accounts = Accounts
                    .Where(x => x.CashAll > balance)
                    .Join(
                        Users,
                        account => account.UserId,
                        user => user.Id,
                        (account, user) => $"{user.FirstName} {user.SurName} > {account.CashAll}"
                     );

                foreach (var account in accounts)
                {
                    Console.WriteLine(account);
                }

            }
            catch
            {
                Console.WriteLine("null");
            }
        }
    }
}