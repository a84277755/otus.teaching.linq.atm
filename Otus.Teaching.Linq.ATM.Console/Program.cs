﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();
            
            // 1
            System.Console.WriteLine("Получим информацию о корректном пользователе:");
            atmManager.GetUserInfo("snow", "111"); // correct
            System.Console.WriteLine("Получим информацию о некорректном пользователе:");
            atmManager.GetUserInfo("snow", "222"); // incorrect

            // 2
            System.Console.WriteLine("Получим счета корректного пользователя:");
            atmManager.GetUserAccounts(1); // correct
            System.Console.WriteLine("Получим счета некорректного пользователя:");
            atmManager.GetUserAccounts(1313531); // correct

            // 3
            System.Console.WriteLine("Получим счета c историей корректного пользователя:");
            atmManager.GetUserAccountsWithHistory(1); // correct
            System.Console.WriteLine("Получим счета c историей некорректного пользователя:");
            atmManager.GetUserAccountsWithHistory(131); // incorrect

            // 4
            System.Console.WriteLine("Получим историю с пользователями:");
            atmManager.GetHistoryWithUserDetails();

            // 5
            System.Console.WriteLine("Получим пользователей с балансом больше 300:");
            atmManager.GetUsersWithSpecificBalance(300);

            System.Console.WriteLine("Получим пользователей с балансом больше 30000:");
            atmManager.GetUsersWithSpecificBalance(30000);


            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}